import pandas as pd
import matplotlib.pyplot as plt
import random
import math


def load_data():
    """
    loading data from csv file

    :return: data
    """
    # Read data from file
    data = pd.read_csv("./data/Dataset2.csv")
    print("Data loaded from file")

    return data


def visualize(data):
    """
    plot data as scatter plot

    :param data: input data
    :return: nothing
    """
    x_list = data['X'].values.tolist()
    y_list = data['Y'].values.tolist()

    plt.scatter(x=x_list, y=y_list)
    plt.show()


def visualize_clusters(data, c_list, k):
    """
    plot data with cluster for data

    :param c_list: list of cluster number for each data
    :param data: input data
    :param k: number of clusters
    :return: nothing
    """

    x_list = data['X'].values.tolist()
    y_list = data['Y'].values.tolist()
    color_list = []

    random_color = random_color_list(k)

    for item in c_list:
        color_list.append(random_color[item])

    plt.scatter(x=x_list, y=y_list, color=color_list)
    plt.show()

def random_color_list(number):
    """
    Generate random list with number items
    :param number: number of items
    :return: random color list
    """

    random_list = []
    for i in range(number):
        color = (random.uniform(0, 1), random.uniform(0, 1), random.uniform(0, 1))
        random_list.append(color)

    return random_list


def k_means(data, k, number_iterations):
    """
    run k-means for clustering data
    :param number_iterations: number of iterations
    :param data: data that has x and y
    :return: cluster of each data, center points
    """
    x_list = data['X'].values.tolist()
    y_list = data['Y'].values.tolist()
    c_list = []

    n = len(x_list)
    center_x = []
    center_y = []

    # initialize centers
    random_list = random.sample(range(0, n), k)

    for i in random_list:
        center_x.append(x_list[i])
        center_y.append(y_list[i])

    for iteration in range(number_iterations):
        # Find cluster of each points
        c_list = []
        for i in range(n):
            best_distance = +1000 * 1000
            best_cluster = -1
            for j in range(k):
                distance = math.hypot(x_list[i] - center_x[j], y_list[i] - center_y[j])
                if distance < best_distance:
                    best_distance = distance
                    best_cluster = j
            c_list.append(best_cluster)

        center_x = []
        center_y = []
        # Calculate new centers
        for j in range(k):
            number_points = 0
            sum_x = 0
            sum_y = 0
            for i in range(n):
                if c_list[i] == j:
                    sum_x += x_list[i]
                    sum_y += y_list[i]
                    number_points += 1

            center_x.append(sum_x / number_points)
            center_y.append(sum_y / number_points)

    return c_list, center_x, center_y


def calculate_cluster_error(data, c_list, center_x, center_y):
    """
    calculating error of clustering

    :param data: data that has x and y
    :param c_list: cluster of each data
    :param center_x: x of center point
    :param center_y: y of center point
    :return: clustering error
    """
    x_list = data['X'].values.tolist()
    y_list = data['Y'].values.tolist()
    n = len(data)
    k = len(center_x)
    cluster_error = []
    cluster_size = []
    for j in range(k):
        cluster_error.append(0)
        cluster_size.append(0)

    for i in range(n):
        cluster_id = c_list[i]
        distance = math.hypot(x_list[i] - center_x[cluster_id], y_list[i] - center_y[cluster_id])
        cluster_size[cluster_id] += 1
        cluster_error[cluster_id] += distance

    for j in range(k):
        cluster_error[j] /= cluster_size[j]

    clustering_error = 0
    for j in range(k):
        clustering_error += cluster_error[j]

    clustering_error /= k
    print("Cluster Error: ", cluster_error)
    print("Clustering Error: ", clustering_error)
    return clustering_error


def normal_rum():
    """
    Run program with a specified "k"
    """
    data = load_data()
    visualize(data=data)
    k = 6
    c_list, center_x, center_y = k_means(data=data, k=k, number_iterations=15)
    visualize_clusters(data=data, c_list=c_list, k=k)
    calculate_cluster_error(data=data, c_list=c_list, center_x=center_x, center_y=center_y)


def elbow_run():
    """
    Run program in a range of k for finding the best "k"
    """
    data = load_data()
    error = []

    max_k = 15

    for k in range(1, max_k):
        c_list, center_x, center_y = k_means(data=data, k=k, number_iterations=15)
        new_error = calculate_cluster_error(data=data, c_list=c_list, center_x=center_x, center_y=center_y)
        error.append(new_error)

    plt.plot(range(1, max_k), error, "xb-")
    plt.show()


if __name__ == '__main__':
    normal_rum()
    # elbow_run()
